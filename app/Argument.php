<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Argument extends Model
{
    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setReasoningAttribute($value)
    {
        $this->attributes['reasoning'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getReasoningAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setCriticalQuestionsAttribute($value)
    {
        $this->attributes['critical_questions'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getCriticalQuestionsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setVariablesAttribute($value)
    {
        $this->attributes['variables'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getVariablesAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Gets all counterarguments. 
     *
     * @return array
     */
    public function getCounterArguments()
    {
        $counter_arguments = Argument::where("parent_id", $this->id)->get()->all();
        if (empty($counter_arguments)) return [];
        $all_c_arguments = [];
        foreach ($counter_arguments as $counter_argument) {
            $c_arguments = $counter_argument->getCounterArguments();
            $all_c_arguments = array_merge($all_c_arguments, $c_arguments);
        }

        $counter_arguments = array_merge($counter_arguments, $all_c_arguments);
        return $counter_arguments;
    }

    /**
     * Builds object representing argument tree. 
     *
     * @return array
     */
    public function getTree($start_node_id = null)
    {
        $tree = [
            "argument" => $this, 
            "children" => [],
            "winning" => true,
            "text" => [
                "name" => $this->title,
            ], 

            "HTMLclass" => "node-winning"
        ];

        $children = Argument::where("parent_id", $this->id)->get();

        if (empty($children))
            return $tree;

        $at_least_one_child_winning = false;
        foreach ($children as $child) {
            $subtree = $child->getTree($start_node_id);

            if ($child->position == "against" && $subtree["winning"])
                $at_least_one_child_winning = true;

            $tree["children"][] = $subtree;
        }  

        $tree["winning"] = (!$at_least_one_child_winning);
        if ($tree["winning"])
            $tree["HTMLclass"] = "node-winning";
        else 
            $tree["HTMLclass"] = "node-losing";

        if ($start_node_id == $this->id)
        	$tree["HTMLclass"] .= " node-highlighted";

        return $tree; 
    }

    /**
     * Gets root for argument tree.  
     *
     * @return Argument
     */
    public function getArgumentRoot()
    {
        $argument = $this;
        while (!empty($argument->parent_id)) {
            $argument = Argument::find($argument->parent_id);
        } 

        return $argument;
    }
}
