<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArgumentationScheme;
use App\Category;
use App\Argument;
use Auth;
use App\Vote;
use App\User;
use Carbon\Carbon;

class ArgumentController extends Controller
{
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Handles argument page. 
     *
     * @param  int  $argument_id
     * @return Response
     */
    public function argument_view($argument_id) {
        $argument = Argument::find($argument_id);
        $user = Auth::user();
        $author = User::find($argument->author_id);
        $questions_votes = [];
        $question_answers = [];
        $question_number_challenges = [];
        $tree = json_encode($argument->getTree($argument->id));

        if (!empty($user)) {
            foreach ($argument->critical_questions as $i => $critical_question) {
                $vote = Vote::where("user_id", $user->id)->where("argument_id", $argument_id)->where("question_index", $i)->first();
                if (!empty($vote)) {
                    $questions_votes[$i] = $vote->option_index;
                }

                $question_answers[$i] = [];
                $sum = 0;
                foreach ($critical_question->options as $j => $option) {
                    $question_answers[$i][$j] = Vote::where("argument_id", $argument_id)->where("question_index", $i)->where("option_index", $j)->get()->count();
                    $sum += $question_answers[$i][$j];
                }

                foreach ($critical_question->options as $j => $option) {
                    if ($sum == 0) 
                        $question_answers[$i][$j] = 0;
                    else 
                        $question_answers[$i][$j] = $question_answers[$i][$j]/$sum*100;
                }

                $question_number_challenges[$i] = Argument::where("parent_id", $argument->id)->where("question_challenged_index", $i)->get()->count();
            }
        }

        $parent = Argument::find($argument->parent_id);
        $question_challenged = "";
        if (!empty($parent)) {
            $tree = json_encode($argument->getArgumentRoot()->getTree($argument->id));
            $question_challenged = $parent->critical_questions[intval($argument->question_challenged_index)]->question;
        }
            
        $counter_arguments = $argument->getCounterArguments();

        foreach ($counter_arguments as $counter_argument) {
            $c_questions_votes = [];
            $c_question_answers = [];
            $c_question_number_challenges = [];

            if (!empty($user)) {
                foreach ($counter_argument->critical_questions as $i => $critical_question) {
                    $vote = Vote::where("user_id", $user->id)->where("argument_id", $counter_argument->id)->where("question_index", $i)->first();
                    if (!empty($vote)) {
                        $c_questions_votes[$i] = $vote->option_index;
                    }

                    $c_question_answers[$i] = [];
                    $sum = 0;
                    foreach ($critical_question->options as $j => $option) {
                        $c_question_answers[$i][$j] = Vote::where("argument_id", $counter_argument->id)->where("question_index", $i)->where("option_index", $j)->get()->count();
                        $sum += $c_question_answers[$i][$j];
                    }

                    foreach ($critical_question->options as $j => $option) {
                        if ($sum == 0) 
                            $c_question_answers[$i][$j] = 0;
                        else 
                            $c_question_answers[$i][$j] = $c_question_answers[$i][$j]/$sum*100;
                    }

                    $c_question_number_challenges[$i] = Argument::where("parent_id", $counter_argument->id)->where("question_challenged_index", $i)->get()->count();
                }
            }
            $counter_argument->questions_votes = $c_questions_votes;
            $counter_argument->question_answers = $c_question_answers;
            $counter_argument->question_number_challenges = $c_question_number_challenges;
        }

        return view('argument.argument', ["argument" => $argument, "counter_arguments" => $counter_arguments, "questions_votes" => $questions_votes, "question_answers" => $question_answers, "parent" => $parent, "question_challenged" => $question_challenged, "question_number_challenges" => $question_number_challenges, "user" => $user, "author" => $author, "tree" => $tree]);
    }

    /**
     * Handles post argument form page. 
     *
     * @param  Request  $request
     * @return Response
     */
    public function post_argument_view(Request $request) {
    	$argumentation_schemes = ArgumentationScheme::all();
        $parent_id = $request->input("parent_id");
        $question_challenged_index = $request->input("question_challenged_index");
        $position = $request->input("position");

        if (!empty($parent_id) && empty($position))
            return redirect()->back();

        if (empty($position))
            $position = "";

        $parent = null;
        if(!empty($parent_id))
            $parent = Argument::find($parent_id);

    	return view('argument.post_argument', ["argumentation_schemes" => $argumentation_schemes, "parent" => $parent, "question_challenged_index" => $question_challenged_index, "position" => $position]);
    }

    /**
     * Handles post argument form submitted. 
     *
     * @param  Request  $request
     * @return Response
     */
    public function post_argument(Request $request) {

    	// Fetching user input.
    	$title = $request->input('title');
    	$category_id = $request->input('category');
    	$argumentation_scheme_id = $request->input('argumentation_scheme');
        $user_variables = json_decode($request->input('user_variables'));
        $logic_steps = json_decode($request->input('logic_steps'));
        $critical_questions = json_decode($request->input('critical_questions'));
        $parent_id = $request->input('parent_id');
        $question_challenged_index = $request->input('question_challenged_index');
        $position = $request->input('position');

        $parent = null;
        if (!empty($parent_id))
            $parent = Argument::find(intval($parent_id));

    	// Validating data. 
    	$errors = [];

    	if (empty($title))
    		$errors[] = 'Please input a title.';	

    	$category = Category::find($category_id);
    	if (empty($category) && empty($parent))
    		$errors[] = 'Please select a Category.';

    	$argumentation_scheme = ArgumentationScheme::find($argumentation_scheme_id);
    	if (empty($argumentation_scheme))
    		$errors[] = 'Please select an Argumentation Scheme.';

    	// If errors are found, redirect back and show errors.
    	if (!empty($errors))
    		return redirect()->back()->with('errors', $errors);

    	// Everything is validated, create argument.
    	$argument = new Argument;
    	$argument->title = $title;
    	$argument->author_id = Auth::user()->id;

        if (empty($parent))
    	   $argument->category_id = $category_id;
        else
            $argument->category_id = $parent->category_id;

    	$argument->argumentation_scheme_id = $argumentation_scheme_id;
        $argument->variables = $user_variables;
        $argument->reasoning = $logic_steps;
        $argument->critical_questions = $critical_questions;

        if (!empty($parent)) {
           $argument->parent_id = $parent->id;
           if (!is_null($question_challenged_index))
               $argument->question_challenged_index = $question_challenged_index;
        }

        if (empty($position))
            $position = "";

        $argument->position = $position;

    	$argument->save();

        if (empty($parent))
            return redirect("/argument/". $argument->id);
        else 
            return redirect("/argument/". $parent->id);
    }

    /**
     * Handles category page. 
     *
     * @param  int $category_id
     * @return Response
     */
    public function category_view($category_id) {
        $arguments = Argument::where("category_id", $category_id)->whereNull("parent_id")->get();
        $category = Category::find($category_id);
        foreach ($arguments as $argument) {
            $argument->author = User::find($argument->author_id);
            $argument->created_at_formatted = (new Carbon($argument->created_at))->diffForHumans();
        }
        return view("category", ["arguments" => $arguments, "category" => $category]);
    }

    /**
     * Handles voting.  
     *
     * @param  Request  $request
     * @param  int      $argument_id
     * @return Response 
     */
    public function register_vote($argument_id, Request $request) {
        $user = Auth::user();
        $question_index = $request->input("question_index");
        $option_index = $request->input("option_index");

        $vote = Vote::where("user_id", $user->id)->where("argument_id", $argument_id)->where("question_index", $question_index)->first();
        if (!empty($vote)) {
            $vote->option_index = $option_index;
            $vote->save();
        } else {
            $vote = new Vote;
            $vote->argument_id = $argument_id;
            $vote->user_id = $user->id;
            $vote->question_index = $question_index;
            $vote->option_index = $option_index;
            $vote->save();
        }

        $argument = Argument::find($argument_id);

        $critical_question = $argument->critical_questions[$question_index];

        $percentages = [];
        $sum = 0;
        foreach ($critical_question->options as $j => $option) {
            $percentages[$j] = Vote::where("argument_id", $argument_id)->where("question_index", $question_index)->where("option_index", $j)->get()->count();
            $sum += $percentages[$j];
        }

        foreach ($critical_question->options as $j => $option) {
            if ($sum == 0) 
                $percentages[$j] = 0;
            else 
                $percentages[$j] = $percentages[$j]/$sum*100;
        }

        return response()->json(["status" => "success", "percentages" => $percentages]);
    }
}