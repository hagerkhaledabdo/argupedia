<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Argument;
use App\Vote;
use DB;
use App\User;

class UserController extends Controller
{
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Handles user private profile page. 
     *
     * @return Response
     */
    public function profile() {
    	$user = Auth::user();
    	$nr_arguments = Argument::where("author_id", $user->id)->whereNull("parent_id")->get()->count();
    	$nr_counter_arguments = Argument::where("author_id", $user->id)->whereNotNull("parent_id")->get()->count();
    	$nr_votes = DB::table("arguments")
    					->join("votes", "arguments.id", "=", "votes.argument_id")
    					->where("arguments.author_id", $user->id)
    					->select("votes.id")
    					->get()
    					->count();

        $arguments = Argument::where("author_id", $user->id)->get();               

    	return view('profile', ["user" => $user, "nr_arguments" => $nr_arguments, "nr_counter_arguments" => $nr_counter_arguments, "nr_votes" => $nr_votes, "arguments" =>$arguments, "auth_user" => $user]);
    }

    /**
     * Handles user public profile page.  
     *
     * @param  int $user_id
     * @return Response
     */
    public function public_profile($user_id) {
        $user = Auth::user();
        $public_user = User::find($user_id);
        $nr_arguments = Argument::where("author_id", $public_user->id)->whereNull("parent_id")->get()->count();
        $nr_counter_arguments = Argument::where("author_id", $public_user->id)->whereNotNull("parent_id")->get()->count();
        $nr_votes = DB::table("arguments")
                        ->join("votes", "arguments.id", "=", "votes.argument_id")
                        ->where("arguments.author_id", $public_user->id)
                        ->select("votes.id")
                        ->get()
                        ->count();

        $arguments = Argument::where("author_id", $public_user->id)->get();               

        return view('profile', ["user" => $public_user, "nr_arguments" => $nr_arguments, "nr_counter_arguments" => $nr_counter_arguments, "nr_votes" => $nr_votes, "arguments" =>$arguments, "auth_user" => $user]);
    }
}
