<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Category;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Used for sending the categories to every view to be displayed in navigation bar. 
     */
    public function __construct() {
    	$mainCategories = Category::orderBy("id", "asc")->limit(5)->get();
    	$moreCategories = Category::orderBy("id", "asc")->offset(5)->limit(PHP_INT_MAX)->get();
    	$categories = Category::orderBy("id", "asc")->get();
    	view()->share("mainCategories", $mainCategories);
    	view()->share("moreCategories", $moreCategories);
    	view()->share("categories", $categories);
    }
}
