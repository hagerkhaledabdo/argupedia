<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArgumentationScheme extends Model
{
    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setReasoningAttribute($value)
    {
        $this->attributes['reasoning'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getReasoningAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setCriticalQuestionsAttribute($value)
    {
        $this->attributes['critical_questions'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getCriticalQuestionsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Set the json encoded value. 
     *
     * @param  array  $value
     * @return void
     */
    public function setVariablesAttribute($value)
    {
        $this->attributes['variables'] = json_encode($value);
    }

    /**
     * Decode json value.
     *
     * @param  string  $value
     * @return array
     */
    public function getVariablesAttribute($value)
    {
        return json_decode($value);
    }
}
