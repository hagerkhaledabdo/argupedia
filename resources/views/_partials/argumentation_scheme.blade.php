<div class="argumentation-layout" id="argumentation-layout-{{ $scheme->id }}">
	<ol class="reasoning">
		@foreach ($scheme->reasoning as $index => $logicStep)
			<li reasoning-index="{{ $index }}" class="logic-step">{{ $logicStep }}</li>
		@endforeach
		<div class="clearfix"></div>
	</ol>
	<br>
	<ol class="critical-questions">
		@foreach ($scheme->critical_questions as $index => $question)
			{{ $index + 1 }}. 
			<li question-index="{{ $index }}" class="critical-question">{{ $question->question }}</li>
			<br>
		@endforeach
	</ol>
</div>