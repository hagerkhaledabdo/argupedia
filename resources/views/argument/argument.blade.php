@extends('layouts.app')

@section('title', $argument->title)

@section('content')

<link rel="stylesheet" href="/js/treant/Treant.css" type="text/css"/>
<link href="/js/treant/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>

<div class="container">
	<div class="row">
		<div class="col-md-12 argument-title-container">
			<div id="tree-container"></div>
			<button id="tree-button" class="btn tree-btn">View Argument Tree</button>
			<h1 class="header-title">{{ $argument->title }} </h1>
			<span class="argument-author">by <a href="/profile/{{ $author->id }}">{{ $author->username }}</a></span>
			@if (!empty($parent)) 
				<div class="based-on-title">
					based on <a href="/argument/{{ $parent->id }}"><i>{{ $question_challenged }}</i></a>
				</div>	
			@endif
		</div>

		<div class="col-md-6">
			<h2 class="c-argument-title">Argument</h2>
			<ol class="argument-reasoning main-argument">
				@foreach ($argument->reasoning as $logicStep) 
					@if(empty($logicStep->rephrasing))
						<li class="argument-logic-step">
							{{ $logicStep->original }}
						</li>
					@else 
						<li class="argument-logic-step">
							{{ $logicStep->rephrasing }}
							<span class="info-button">
								i
								<div class="info-body">
									<span class="info-explanation">Structured Version:</span><br>
									{{ $logicStep->original }}
								</div>
							</span>
						</li>
					@endif
				@endforeach	
			</ol>
			<hr>
			<h2 class="c-argument-title">Critical Questions</h2>
			<ol class="main-argument">
				@foreach ($argument->critical_questions as $i => $criticalQuestion) 
					<li class="argument-critical-question">
						{{ $criticalQuestion->question }}
						<br>
						@if (!empty($criticalQuestion->options))
							@foreach ($criticalQuestion->options as $j => $option)
								{!! Form::open(["url" => "/argument/".$argument->id."/vote", "method" => "post", "class" => "option-form"]) !!}
									<input type="hidden" name="question_index" value="{{ $i }}">
									<input type="hidden" name="option_index" value="{{ $j }}">
									@if(isset($questions_votes[$i])) 
										<span class="answer-percentage" option-index="{{ $j }}">{{ floor($question_answers[$i][$j]) }}%</span>
									@else
										<span class="answer-percentage" option-index="{{ $j }}"></span>
									@endif
									<button type="submit" class="btn btn-info option-btn" @if(isset($questions_votes[$i]) && $questions_votes[$i] == $j) disabled @endif @if(empty($user) || (!empty($user) && $user->id == $argument->author_id)) disabled @endif>{{ $option }}</button>
								{!! Form::close() !!}
							@endforeach
						@endif
						
						<span class="number-challenges">{{ isset($question_number_challenges[$i]) ? $question_number_challenges[$i] : 0 }}</span>
						
						@if(empty($user) || (!empty($user) && $user->id == $argument->author_id))
							<button class="btn mustard-btn" id="challenge-btn" disabled>Challenge Argument</button>
							<button class="btn mustard-btn" id="support-btn" disabled>Support Argument</button>
						@else
							<a href="/post-argument?parent_id={{ $argument->id }}&question_challenged_index={{ $i }}&position=against"><button class="btn mustard-btn" id="challenge-btn">Challenge Argument</button></a>
							<a href="/post-argument?parent_id={{ $argument->id }}&question_challenged_index={{ $i }}&position=for"><button class="btn mustard-btn" id="support-btn">Support Argument</button></a>
						@endif	
					</li>
				@endforeach	
			</ol>
		</div>

		<div class="col-md-6">

			<h2 class="c-argument-header">Child Arguments</h2>
			<div id="counter-argument-lists">
				@if (empty($counter_arguments)) 
					<div class="no-posts">
						No children arguments posted.
					</div>	
				@endif

				@foreach ($counter_arguments as $counter_argument) 
				<div class="counter-argument-box">
					<ul class="nav nav-tabs" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link active" id="argument-tab-{{ $counter_argument->id }}" data-toggle="tab" href="#argument-{{ $counter_argument->id }}" role="tab" aria-controls="argument-{{ $counter_argument->id }}" aria-selected="true">Argument</a>
					  </li>

					  <li class="nav-item">
					    <a class="nav-link" id="critical-questions-tab-{{ $counter_argument->id }}" data-toggle="tab" href="#critical-questions-{{ $counter_argument->id }}" role="tab" aria-controls="critical-questions-{{ $counter_argument->id }}" aria-selected="false">Critical Questions</a>
					  </li>
					  
					</ul>
					<div class="tab-content">
					  <div class="tab-pane fade show active counter-main" id="argument-{{ $counter_argument->id }}" role="tabpanel" aria-labelledby="argument-tab-{{ $counter_argument->id }}">
					  		
					  		<h2 class="c-argument-title"><a href="/argument/{{ $counter_argument->id }}">{{ $counter_argument->title }}</a></h2>
							<ol class="argument-reasoning">
								@foreach ($counter_argument->reasoning as $logicStep) 
									@if(empty($logicStep->rephrasing))
										<li class="argument-logic-step">
											{{ $logicStep->original }}
										</li>
									@else 
										<li class="argument-logic-step">
											{{ $logicStep->rephrasing }}
											<span class="info-button">
												i
												<div class="info-body">
													<span class="info-explanation">Structured Version:</span>
													{{ $logicStep->original }}
												</div>
											</span>
										</li>
									@endif
								@endforeach	
							</ol>

					  </div>
					  <div class="tab-pane fade counter-main" id="critical-questions-{{ $counter_argument->id }}" role="tabpanel" aria-labelledby="critical-questions-tab-{{ $counter_argument->id }}">

						<ol>
							@foreach ($counter_argument->critical_questions as $i => $criticalQuestion) 
								<li class="argument-critical-question">
									{{ $criticalQuestion->question }}
									<br>
									@if (!empty($criticalQuestion->options))
										@foreach ($criticalQuestion->options as $j => $option)
											{!! Form::open(["url" => "/argument/".$counter_argument->id."/vote", "method" => "post", "class" => "option-form"]) !!}
												<input type="hidden" name="question_index" value="{{ $i }}">
												<input type="hidden" name="option_index" value="{{ $j }}">
												@if(isset($counter_argument->questions_votes[$i])) 
													<span class="answer-percentage" option-index="{{ $j }}">{{ floor($counter_argument->question_answers[$i][$j]) }}%</span>
												@else 
													<span class="answer-percentage" option-index="{{ $j }}"></span>
												@endif
												<button type="submit" class="btn btn-info option-btn" @if(isset($counter_argument->questions_votes[$i]) && $counter_argument->questions_votes[$i] == $j) disabled @endif @if(empty($user) || (!empty($user) && $user->id == $counter_argument->author_id)) disabled @endif>{{ $option }}</button>
											{!! Form::close() !!}
										@endforeach
									@endif
									
									<span class="number-challenges">{{ isset($counter_argument->question_number_challenges[$i]) ? $counter_argument->question_number_challenges[$i] : 0 }}</span>
									
									@if(empty($user) || (!empty($user) && $user->id == $argument->author_id))
										<button class="btn mustard-btn" id="challenge-btn" disabled>Challenge Argument</button>
										<button class="btn mustard-btn" id="support-btn" disabled>Support Argument</button>
									@else
										<a href="/post-argument?parent_id={{ $counter_argument->id }}&question_challenged_index={{ $i }}&position=against"><button class="btn mustard-btn" id="challenge-btn">Challenge Argument</button></a>
										<a href="/post-argument?parent_id={{ $counter_argument->id }}&question_challenged_index={{ $i }}&position=for"><button class="btn mustard-btn" id="support-btn">Support Argument</button></a>
									@endif
								</li>
							@endforeach	
						</ol>
					  </div>
					</div>
				</div>	
				@endforeach

			</div>
		</div>
	</div>
</div>

<script>
	let chart_config = {
		chart: {
			container: "#tree-container", 
			scrollbar: "fancy",
			node: {
				HTMLclass: "node-argument",
				collapsable: true
			}
		}, 
		nodeStructure: {!! $tree !!}
	}
</script>

<script src="/js/treant/vendor/jquery.mousewheel.js"></script>
<script src="/js/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="/js/treant/vendor/raphael.js"></script>
<script src="/js/treant/Treant.js"></script>

<script src="/js/argument.js"></script>

@endsection