@extends('layouts.app', ["nonav" => true, 'nobuttons' =>true])

@section('title', 'Post Argument')

@section('content')

<div class="separator"></div>

<div class="container" id="post-argument-container">
	<a href="javascript:history.back()" id="cancel-argument"><i class="fas fa-times"></i></a> 
	@if (empty($parent)) 
		<h2>New Argument</h2>
	@else
		<h2>New 
			@if ($position == "against")
				Challenge
			@else 
				Support	
			@endif
		 Argument <small>for <a href="/argument/{{ $parent->id }}">{{ $parent->title }}</a></small></h2> 
	@endif		

	{!! Form::open([ 'url' => '/post-argument', 'method' => 'post', 'class' => 'form', 'id' => 'post-argument-form' ]) !!}

		@if (Session::has('errors')) 
			<div class="alert alert-danger col-md-10">{!! implode('<br>', Session::get('errors')) !!}</div>
		@endif 
	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group col-md-10">
					<label>Argument Title</label>
					<input type="text" name="title" class="form-control">
				</div>
				
				<div class="form-group col-md-10" @if (!empty($parent)) style="display: none;" @endif>
					<label>Category</label>
					<select name="category" class="form-control">
						<option value="-1" disabled selected>Choose One</option>
						@foreach ($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-10">
					<label>Argumentation Scheme</label>
					<select name="argumentation_scheme" class="form-control" id="argumentation-scheme-select">
						<option value="-1" disabled selected>Choose One</option>
						@foreach ($argumentation_schemes as $argumentation_scheme)
							<option value="{{ $argumentation_scheme->id }}">{{ $argumentation_scheme->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="col-md-6">
				@foreach ($argumentation_schemes as $argumentation_scheme)
					@include ("_partials.argumentation_scheme", ["scheme" => $argumentation_scheme])
				@endforeach
			</div>
		</div>

		<div class="clearfix"></div>

		<input type="hidden" id="user-variables" name="user_variables">
		<input type="hidden" id="logic-steps" name="logic_steps">
		<input type="hidden" id="critical-questions" name="critical_questions">
		<input type="hidden" name="position" value="{{ $position }}">

		@if (!empty($parent))
			<input type="hidden" name="parent_id" value="{{ $parent->id }}">
		@endif	

		@if (!is_null($question_challenged_index))
			<input type="hidden" name="question_challenged_index" value="{{ $question_challenged_index }}">
		@endif

		<div class="form-group col-md-12">
			<button type="submit" class="btn mustard-btn disabled" id="post-argument-form-button" rel="tooltip" data-title="Please fill in all the variables.">Submit</button>
			<button class="btn btn-info d-none" id="rephrase-button">Would you like to rephrase?</button>
		</div>

	{!! Form::close() !!}

</div>

<script>
	let argumentationSchemes = {!! json_encode($argumentation_schemes) !!};
</script>

<script src="/js/post_argument.js"></script>    
@endsection