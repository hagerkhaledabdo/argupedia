@extends('layouts.app', ["category_id" => $category->id])

@section('title', $category->name)

@section('content')

<div class="container">
	<div class="row">
		@foreach ($arguments as $argument)
			<div class="col-md-4 category-arguments-wrapper">
				<div class="category-arguments">
					<h1 class="argument-title">
						<a href="/argument/{{ $argument->id }}">
							{{ $argument->title }}
						</a>
					</h1>
					<p class="author-date">
						by: <a href="/profile/{{ $argument->author_id }}">{{ $argument->author->username }}</a>
						<br>
						{{ $argument->created_at_formatted }}
					</p>
				</div>	
			</div>
		@endforeach	
	</div>
</div>

@endsection