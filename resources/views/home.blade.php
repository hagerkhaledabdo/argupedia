@extends('layouts.app', ["page" => "homepage"])

@section('title', 'Home Page')

@section('content')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
  	<div class="carousel-item active">
  	  <h1 class="welcome-header">Welcome to Argupedia</h1>
  	  <p class="paragraph-header">Here is a brief guideline providing you with all the information. Let's get started!</p>
  	</div>
    <div class="carousel-item" style="background:url(/images/slide_one.png);background-position: center; background-size: contain; background-repeat: no-repeat;">
    </div>
    <div class="carousel-item" style="background:url(/images/slide_two.png);background-position: center; background-size: contain; background-repeat: no-repeat;">
    </div>
    <div class="carousel-item" style="background:url(/images/slide_three.png);background-position: center; background-size: contain; background-repeat: no-repeat;">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    
@endsection