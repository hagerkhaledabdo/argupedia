@extends('layouts.app', ["nonav" => true])

@section('title', 'Profile')

@section('content')

<div class="container">	

	<div class="col-md-12">
		<div id="user-initial">
			{{ $user->username[0] }}
		</div>
	</div>

	<div class="col-md-12">
		<div id="user-name">
			{{ $user->name }}
		</div>
	</div>

	<div class="col-md-12">
		<div id="statistics">
			<div class="stat">
				<span class="value">{{ $nr_votes }}</span>
				<span class="label">Votes</span>
			</div>
			<div class="stat">
				<span class="value">{{ $nr_counter_arguments }}</span>
				<span class="label">Counter-Arguments</span>
			</div>
			<div class="stat">
				<span class="value">{{ $nr_arguments }}</span>
				<span class="label">Arguments</span>
			</div>
		</div>
	</div>

	<div id="arguments-list" class="row">
		@if (!empty($auth_user) && $auth_user->id == $user->id)
		<div class="col-md-4 argument-box-wrapper">
			<a href="{{ url('post-argument') }}" class="argument-box post-argument-box">Post New Argument <i class="fas fa-plus-circle argument-icon"></i></a>
		</div>	
		@endif	
		@foreach ($arguments as $argument) 
		<div class="col-md-4 argument-box-wrapper">
			<a href="/argument/{{ $argument->id }}" class="argument-box existing-argument-box">
				{{ $argument->title }} <i class="fas fa-eye argument-icon"></i>
			</a>
		</div>	
		@endforeach
	</div>

</div>
    
@endsection