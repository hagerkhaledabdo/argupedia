<!DOCTYPE html>
<html lang="en" class="@if (isset($page) && $page=="homepage") fullheight @endif">

<head>
	<meta charset="UTF-8">
	<title>Argupedia | @yield('title')</title>
	<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.min.css">
	<!-- font stylesheet-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<link rel="stylesheet" href="/css/style.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="/lib/bootstrap/js/bootstrap.min.js"></script>

	<script src="/js/main.js"></script>
</head>

<body class="@if (isset($page) && $page=="homepage") fullheight @endif">

	<section id="cover">
		<div id="cover-caption">
			<div class="container">
				<div class="logo-img-container">
					<img src="/images/logo.png" class="logo-img">
				</div>
				<div class="col-sm-10 col-sm-offset-1"></div>
				<div class="left-header">
					<h1 class="logo"><a href="/">Argupedia</a></h1>
					<p class="motto">Arguing, let's add some sense.</p>
				</div>
				
				@if (!isset($nobuttons) || (isset($nobuttons) && $nobuttons == false))

				<div class="login-container">
					@if (Auth::guest())
						<a href="{{ url('login') }}"><button type="button" class="btn register-btn">Login</button></a>
						<a href="{{ url('register') }}"><button type="button" class="btn register-btn">Sign Up</button></a>
					@else 
						<button type="button" class="btn profile-btn"><span id="profile-icon"><a href="/profile"><i class="far fa-user"></i> {{ Auth::user()->name }}</a></span></button>

						{!! Form::open(["url" => "/logout", "method" => "post", "id" => "logout-form"]) !!}
							<button class="btn btn-dark logout" type="submit">Logout</button>
						{!! Form::close() !!}
					@endif 		
				</div>

				@endif

				<div class="clearfix"></div>
			</div>
		</div>
	</section>

	@if (!isset($nonav) || (isset($nonav) && $nonav == false)) 

	<nav class="navbar navbar-expand-lg navbar-dark toolbar">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		</button>

		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item @if (isset($page) && $page=="homepage") active @endif">
					<a class="nav-link" href="/">Home</a>
				</li>

				@foreach ($mainCategories as $mainCategory)

					<li class="nav-item @if (isset($category_id) && $category_id==$mainCategory->id) active @endif">
						<a class="nav-link" href="/category/{{ $mainCategory->id }}">{{ $mainCategory->name }}</a>
					</li>

				@endforeach	

				<li class="nav-item dropdown">
					<a class= "nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

						@foreach ($moreCategories as $moreCategory)

							<a class="dropdown-item" href="/category/{{ $moreCategory->id }}">{{ $moreCategory->name }}</a>

						@endforeach

					</div>
				</li>
			</ul>
		</div>	
	</nav>
		
	@endif

	@yield('content')		 

	<footer class="toolbar @if (isset($page) && $page=="homepage") footerhomepage @endif">
		<p>&copy; 2018 Argupedia</p>
	</footer>

</body>
</html>