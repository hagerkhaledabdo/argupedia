<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IntroController@index');

Route::middleware(['auth'])->group(function () {

	Route::get('/profile', 'UserController@profile');
	Route::get('/post-argument', 'ArgumentController@post_argument_view');
	Route::post('/post-argument', 'ArgumentController@post_argument');
	Route::post('/argument/{argument_id}/vote', 'ArgumentController@register_vote');

});

Route::get('/argument/{argument_id}', 'ArgumentController@argument_view');
Route::get('/category/{category_id}', 'ArgumentController@category_view');
Route::get('/profile/{user_id}', 'UserController@public_profile');

Auth::routes();

