### Server Requirements
 - PHP >= 7.0.0
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Mbstring PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension

### Set up project

1. Clone repository 
```sh
$ git clone https://hagerkhaledabdo@bitbucket.org/hagerkhaledabdo/argupedia.git
```

Note: all the commands from the following steps are run from the root directory of the application

2. Run from the terminal:
```sh
$ composer install
```

3. Set up directory permissions. Directories within the storage and the bootstrap/cache directories should be writable by your web server. For the purpose of testing, the following commands can be used, if on an Unix-based environment:
```sh
$ sudo chmod -R 777 storage
$ sudo chmod -R 777 bootstrap/cache
```

4. Create a .env file in the root of your application with the following contents, replacing some variables according to your system
```sh
APP_NAME=Argupedia
APP_ENV=local
APP_KEY=base64:3TmdWtNjRZv/HzrzBFjBiy1wBSPSEL/o3LjhWn1G66g=
APP_DEBUG=true
APP_LOG=daily
APP_LOG_LEVEL=debug
APP_URL=http://yourdomain.com

DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=/path/to/database/database.sqlite
DB_USERNAME=your_db_username
DB_PASSWORD=your_db_password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1
```

4. Run from the terminal:
```sh
$ php artisan key:generate
```
5. For rapid deployment sqlite can be used. Start by creating a file in the database directory named database.sqlite. For Unix-based systems:
```sh
$ touch database/database.sqlite
```

6. Make sure the following attributes are set correctly in the .env file (replace DB_DATABASE with the path to the sqlite file)
```sh
DB_CONNECTION=sqlite
DB_DATABASE=/path/to/database/database.sqlite
```

7. Run from the terminal:
```ssh
$ php artisan migrate
```
8. Run from the terminal:
```sh
$ php artisan db:seed
```

9. Run from the terminal:
```sh
$ php artisan serve
```

10. Done. Follow the url displayed on screen to browse the website. It should look like something like this:
```sh
Laravel development server started: <http://127.0.0.1:8000>
```