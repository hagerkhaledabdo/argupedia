$(function(){

	let userVariables = {};
	let logicSteps = [];
	let criticalQuestions = [];
	let initialSchemeLayouts = {};

	let selectedScheme = null;
	let $selectedSchemeLayout = null;

	$(document).ready(function(){
		handleButtonTooltips();
		handleArgumentationSchemeVariables();
		handleArgumentationSchemeSelect();
		syncArgumentationSchemeVariables();
		handleRephrasing();
		handleOptions();
		handleFormSubmit();
	});

	// Sets up the layout of the post argument form when an argumentation scheme is selected. 
	function handleArgumentationSchemeSelect(){
		$("#argumentation-scheme-select").on("change", function (){
			if ($selectedSchemeLayout != null) {
				$reasoningRephrasingBox = $selectedSchemeLayout.find(".reasoning-rephrasing").first();

				if ($reasoningRephrasingBox.length) {
					$reasoningBox = $selectedSchemeLayout.find(".reasoning").first();
					$reasoningRephrasingBox.find(".logic-step").each(function(){
						let reasoningIndex = parseInt($(this).attr("reasoning-index"));
						logicSteps[reasoningIndex].rephrasing = "";
					});
					$selectedSchemeLayout.find(".var").attr("contenteditable", "true").removeClass("var-disable");
					$selectedSchemeLayout.find(".option").removeClass("option-disabled");
					$selectedSchemeLayout.find(".critical-questions .var, .var-option").removeAttr("contenteditable");

					let $rephraseButtonDetach = $("#rephrase-button").detach();
					$reasoningRephrasingBox.remove();
					$rephraseButtonDetach.text("Rephrase").attr("cancel", "false");
					$rephraseButtonDetach.insertBefore($selectedSchemeLayout.find(".reasoning .clearfix").first());
				}
			}
			
			let $rephraseButton = $("#rephrase-button").detach();
			let id = $(this).val();
			$selectedSchemeLayout = $("#argumentation-layout-" + id);
			$selectedSchemeLayout.html(initialSchemeLayouts[id]);

			if ($rephraseButton.attr("cancel") == "true") {
				$rephraseButton.text("Rephrase").attr("cancel", "false");
			}

			$rephraseButton.insertBefore($selectedSchemeLayout.find(".reasoning .clearfix").first()).addClass("d-none");
			$(".argumentation-layout").css("display", "none");
			$("#argumentation-layout-" + id).css("display", "block");
			let argumentationScheme = getArgumentationSchemeByID(id);
			selectedScheme = argumentationScheme;

			userVariables = {};
			for (let j = 0; j < argumentationScheme.variables.length; j++) {
				userVariables[argumentationScheme.variables[j]] = "";
			}

			logicSteps = [];
			for (let j = 0; j < argumentationScheme.reasoning.length; j++) {
				logicSteps.push({
					original: $selectedSchemeLayout.find('li[reasoning-index="'+ j +'"]').text(), 
					rephrasing: "", 
					HTML: "" 
				});
			}

			criticalQuestions = [];
			for (let j = 0; j < argumentationScheme.critical_questions.length; j++) {
				criticalQuestions.push({
					question: $selectedSchemeLayout.find('li[question-index="'+ j +'"]').text(),
					options: argumentationScheme.critical_questions[j].options
				});
			}
		});	 
	}

	// Replaces variable tags that were established by convention with actual html tags to make user variables editable. 
	function handleArgumentationSchemeVariables(){
		for (let i = 0; i < argumentationSchemes.length; i++) {

			let argumentationScheme = argumentationSchemes[i];
			let layoutHTML = $("#argumentation-layout-" + argumentationScheme.id).html();

			for (let j = 0; j < argumentationScheme.variables.length; j++) {
				let variable = argumentationScheme.variables[j];
				let openingTag = "&lt;" + variable + "&gt;";
				let closingTag = "&lt;/" + variable + "&gt;";
				let reg1 = new RegExp(openingTag, "g");
				let reg2 = new RegExp(closingTag, "g");

				if(variable.indexOf("OPTION") == -1) {
					layoutHTML = layoutHTML.replace(reg1, '<span contenteditable class="var" variable-name="'+ variable + '">');
					layoutHTML = layoutHTML.replace(reg2, '</span>');
				} else {
					layoutHTML = layoutHTML.replace(reg1, '<span class="var var-option" variable-name="'+ variable + '">');
					layoutHTML = layoutHTML.replace(reg2, '</span>');
				}	
			}

			$("#argumentation-layout-" + argumentationScheme.id).html(layoutHTML);
			$("#argumentation-layout-" + argumentationScheme.id).find(".critical-questions [contenteditable]").removeAttr("contenteditable");

			$(".var-option").each(function(){
				let options = $(this).text().split("/");

				for (let i = 0; i < options.length; i++) {
					options[i] = '<span class="option">' + options[i] + "</span>";	
				}

				options = options.join("/");
				$(this).html(options);
			});

			initialSchemeLayouts[argumentationScheme.id] = $("#argumentation-layout-" + argumentationScheme.id).html();
		}
	}

	// Syncs the same variables together so when one changes, all change. 
	function syncArgumentationSchemeVariables(){
		$(document).on("keyup", ".var", function (){
			let variable = $(this).attr("variable-name");
			let layout = $(this).parents(".argumentation-layout").first();
			let current = $(this);
			layout.find('.var[variable-name="'+ variable + '"]').each(function(){
				if(!$(this).is(current))
					$(this).text(current.text());
			});	
			userVariables[variable] = $(this).text();

			layout.find(".logic-step").each(function(){
				let logicStepIndex = $(this).attr("reasoning-index");
				if($("#rephrase-button").attr("cancel") != "true"){
					logicSteps[logicStepIndex].original = getLogicStepText(logicStepIndex);
				}
			});

			layout.find(".critical-question").each(function(){
				let questionIndex = $(this).attr("question-index");
				if($("#rephrase-button").attr("cancel") != "true"){
					criticalQuestions[questionIndex].question = $selectedSchemeLayout.find('li[question-index="'+ questionIndex +'"]').text();
				}
			});

			if (checkVariablesCompletion())
				enableButtons();
			else 
				disableButtons();
		});
	}

	// Fetches argumentation scheme object by id. 
	function getArgumentationSchemeByID(scheme_id){
		for (let i = 0; i < argumentationSchemes.length; i++) {
			if(argumentationSchemes[i].id == scheme_id)
				return argumentationSchemes[i];
		}
		return null;
	}

	// Handles the functionality that allows the user to input a rephrased structure. 
	function handleRephrasing(){
		$("#rephrase-button").on("click", function(event){

			event.preventDefault();
			$button = $(this).detach();

			if($button.attr("cancel") != "true"){
				$selectedSchemeLayout.find(".var").removeAttr("contenteditable").addClass("var-disable");
				$selectedSchemeLayout.find(".option").addClass("option-disabled");

				$reasoningBox = $selectedSchemeLayout.find(".reasoning").first();
				$reasoningRephrasingBox = $reasoningBox.clone(true).addClass("reasoning-rephrasing");
				$reasoningRephrasingBox.insertAfter($reasoningBox);

				$reasoningRephrasingBox.find(".logic-step").attr("contenteditable", "true").addClass("logic-step-editable");
				$button.text("Cancel").attr("cancel", "true");
				$button.insertBefore($selectedSchemeLayout.find(".reasoning-rephrasing .clearfix").first());
				// $selectedSchemeLayoutRephrasing.find(".logic-step").each(function(){
				// 	let reasoningIndex = parseInt($(this).attr("reasoning-index"));
				// 	logicSteps[reasoningIndex].HTML = $(this).html();
				// });
			} else {
				$reasoningRephrasingBox = $selectedSchemeLayout.find(".reasoning-rephrasing").first();
				$reasoningBox = $selectedSchemeLayout.find(".reasoning").first();
				$reasoningRephrasingBox.find(".logic-step").each(function(){
					let reasoningIndex = parseInt($(this).attr("reasoning-index"));
					// $(this).html(logicSteps[reasoningIndex].HTML);
					logicSteps[reasoningIndex].rephrasing = "";
				});
				$selectedSchemeLayout.find(".var").attr("contenteditable", "true").removeClass("var-disable");
				$selectedSchemeLayout.find(".option").removeClass("option-disabled");
				$selectedSchemeLayout.find(".critical-questions .var, .var-option").removeAttr("contenteditable");
				// $selectedSchemeLayout.find(".logic-step").removeAttr("contenteditable").removeClass("logic-step-editable");
				$reasoningRephrasingBox.remove();
				$button.text("Rephrase").attr("cancel", "false");
				$button.insertBefore($selectedSchemeLayout.find(".reasoning .clearfix").first());
			}	
		});

		$(document).on("keyup", ".logic-step:not(.var)", function(){
			let logicStepIndex = $(this).attr("reasoning-index");
			if($("#rephrase-button").attr("cancel") == "true"){
				logicSteps[logicStepIndex].rephrasing = $(this).text();
			}
		});
	}

	// Gives functionality to the mulptiple choice option type variables. 
	function handleOptions(){
		$(document).on("click", ".option:not(.option-disabled)", function(){
			$(this).siblings(".option-selected").removeClass("option-selected");
			$(this).addClass("option-selected");

			let variable = $(this).parents(".var").attr("variable-name");
			userVariables[variable] = $(this).text();

			let layout = $(this).parents(".argumentation-layout").first();
						layout.find(".logic-step").each(function(){
							let logicStepIndex = $(this).attr("reasoning-index");
							if($("#rephrase-button").attr("cancel") != "true"){
								logicSteps[logicStepIndex].original = getLogicStepText(logicStepIndex);
							}
						});

			if (checkVariablesCompletion())
				enableButtons();
			else 
				disableButtons();
		});
	}

	// Gets the text from the html of a logic step in order to save it in memory. 
	function getLogicStepText(logicStepIndex){
		let text = selectedScheme.reasoning[logicStepIndex];

		for (let variable in userVariables) {
    		if (userVariables.hasOwnProperty(variable)) {
    			let openingTag = "<" + variable + ">";
    			let closingTag = "</" + variable + ">";

    			if (userVariables[variable] != "") {
	    			let reg = new RegExp(openingTag + ".*" + closingTag, "g");
	    			text = text.replace(reg, userVariables[variable]);
	    		} else {
	    			let reg1 = new RegExp(openingTag, "g");
	    			let reg2 = new RegExp(closingTag, "g");
	    			text = text.replace(reg1, "");
	    			text = text.replace(reg2, "");
	    		}
   			}
		}	
		return text;		
	}

	// Hijacks submit form in order to inject the variables stored in memory inside the request before submitting again.
	function handleFormSubmit(){
		$("#post-argument-form").on("submit", function(event){
			event.preventDefault();

			if (!checkVariablesCompletion())
				return false;

			$.each(logicSteps, function(){
				delete this.HTML;
			});

			$("#user-variables").val(JSON.stringify(userVariables));
			$("#logic-steps").val(JSON.stringify(logicSteps));
			$("#critical-questions").val(JSON.stringify(criticalQuestions));
			$("#post-argument-form").off("submit");
			$(this).submit();
		});
	}

	// Handles general tooltips. 
	function handleButtonTooltips(){
		$('body').tooltip({
    		selector: '[rel="tooltip"]'
		});

		$(".btn").click(function(e) {
		    if ($(this).hasClass("disabled"))
		    {
		    	e.preventDefault();
		    	return false;
		    }
		});
	}

	// Enables the rephrase and submit buttons. 
	function enableButtons(){
		$("#post-argument-form-button").removeClass("disabled").attr("rel", null);
		$("#rephrase-button").removeClass("d-none");
	}

	// Disables the rephrase and submit button. 
	function disableButtons(){
		$("#post-argument-form-button").addClass("disabled").attr("rel", "tooltip");
		$("#rephrase-button").addClass("d-none");
	}

	// Checks that all the variables have been inputted by the user. 
	function checkVariablesCompletion(){
		for (let j = 0; j < selectedScheme.variables.length; j++) {
			if (userVariables[selectedScheme.variables[j]] == "")
			{
				return false;
			}	
		}
		return true;
	}
});