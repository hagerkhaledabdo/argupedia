$(function(){

	$(document).ready(function(){
		handleRephrasingTooltips();
		let tree = new Treant(chart_config);
		handleTreeButton();
		$("#tree-container").css("display", "none");
		$("#tree-container").css("opacity", "1");
		handleVotesAjax();
	});

	// Listens when user hovers their mouse over information tool tips, for layout of structured argument. 
	function handleRephrasingTooltips() {
		$(document).on("mouseenter", ".info-button", function(){
			$(this).find(".info-body").css("display", "block");

			let $this_button = $(this);
			$(".info-button").each(function(){
				if(!$(this).is($this_button)) $(this).css("display", "none");
			});
		});

		$(document).on("mouseout", ".info-button", function(){
			$(this).find(".info-body").css("display", "none");

			let $this_button = $(this);
			$(".info-button").each(function(){
				if(!$(this).is($this_button)) $(this).css("display", "block");
			});
		});
	}

	// Listens when user clicks on view argument tree. 
	function handleTreeButton() {
		$(document).on("click", "#tree-button", function(){
			if ($(this).attr("clicked") != "true") { 
				$("#tree-container").css("top", "0px");
				$("#tree-container").css("left", "0px");
				$("#tree-container").css("display", "block");
				$(this).attr("clicked", "true");
				$(this).text("Close Argument Tree");
			} else {
				$("#tree-container").css("display", "none");
				$(this).attr("clicked", "false");
				$(this).text("View Argument Tree");
			}	
		});
	}

	// Sends asynchronous request to server to regiter user votes and updates result percentages. 
	function handleVotesAjax() {
		$(document).on("submit", ".option-form", function(e){
			e.preventDefault();
			$this_form = $(this);

			$.ajax({
				url: $(this).attr("action"), 
				method: "post",
				data: $(this).serialize(),
				success: function(response){
					console.log(response);
					$this_form.parents(".argument-critical-question").first().find(".option-btn").removeAttr("disabled");
					$this_form.find(".option-btn").attr("disabled", "disabled");

					for (let i = 0; i < response.percentages.length; i++) {
						$this_form.parents(".argument-critical-question").first().find('.answer-percentage[option-index="' + i + '"]').first().text(Math.floor(response.percentages[i]) + "%");
					}
				},
				error: function(jqxhr,responseText){
					console.log(jqxhr);
					console.log(responseText);
				}
			})
		});
	}

});

