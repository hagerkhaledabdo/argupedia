$(function(){

	$(document).ready(function(){

		checkFooterPosition();

	});

	$(window).resize(checkFooterPosition);

	// Correctly positions footer when page is being resized. 
	function checkFooterPosition() {

		if ($('body').outerHeight() < $(window).outerHeight())
			$('footer').addClass('bottom-fixed');
	}

});