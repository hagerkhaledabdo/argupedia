<?php

use Illuminate\Database\Seeder;

class InitialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('argumentation_schemes')->insert([
            'id' => 1,
            'name' => 'Expert Opinion',
            'reasoning' => '["<E>Source E</E> is an expert in <D>subject domain D</D> containing <S>proposition S</S>.", "<E>E</E> asserts <A>action A</A> (in <D>domain D</D>) is <OPTION1>true/false</OPTION1>.", "<S>S</S> may plausibly be taken to be <OPTION2>true/false</OPTION2>."]',
            'created_at' => '2018-02-04 14:02:08',
            'updated_at' => '2018-02-04 14:02:08',
            'critical_questions' => '[{"question":"How credible is <E>E</E> as an expert?","options":["Credible","Not Credible"]},{"question":"Is <E>E</E> an expert in the field that <S>S</S> is in?","options":["Agree","Disagree"]},{"question":"What did <E>E</E> assert that implies <S>S</S>?","options":[]},{"question":"Is <E>E</E> personally reliable and trustworthy? Do we have any reason to think <E>E</E> is less than honest?","options":["Trustworthy","Not Trustworthy"]},{"question":"Is <S>S</S> consistent with what other experts assert?","options":["Agree","Disagree"]},{"question":"Is <E>E</E>’s evidence based on evidence?","options":["Evidence","No Evidence"]}]',
            'variables' => '["E", "D", "A", "S", "OPTION1", "OPTION2"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 3,
            'name' => 'Position to Know',
            'reasoning' => '["<a>a</a> is in a position to know whether <A>A</A> is <OPTION1>true/false</OPTION1>.", "<a>a</a> asserts that <A>A</A> is <OPTION2>true/false</OPTION2>.", "<A>A</A> may plausibly be taken to be <OPTION3>true/false</OPTION3>."]',
            'critical_questions' => '[{"question":"Is <a>a</a> in a position to know whether <A>A</A> is true?","options":["Agree","Disagree"]},{"question":"What is it about <a>a</a> that makes them likely to know about <A>A</A>?","options":[]},{"question":"Is <a>a</a> an honest, trustworthy and reliable source?","options":["Agree","Disagree"]},{"question":"Would <a>a</a> have any reason to mislead us?","options":[]},{"question":"Did <a>a</a> really assert that <A>A</A> is true or false?","options":["True","False"]},{"question":"Are we bearing what <A>A</A> said first-hand or second-hand? Do we have reason to be suspicious about the fidelity of the information-transfer?","options":["First-Hand","Second-Hand"]}]',
            'variables' => '["a", "A", "OPTION1", "OPTION2", "OPTION3"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 4,
            'name' => 'Analogy',
            'reasoning' => '["Generally, <C1>case C1</C1> is similar to <C2>case C2</C2>.", "<A>A</A> is <OPTION1>true/false</OPTION1> in <C1>case C1</C1>.", "<A>A</A> is probably <OPTION2>true/false</OPTION2> in <C2>case C2</C2>."]',
            'critical_questions' => '[{"question":"Are <C1>C1</C1> and <C2>C2</C2> similar, in the respect cited?","options":["Agree","Disagree"]},{"question":"Is <A>A</A> true or false in <C1>C1</C1>?","options":["True","False"]},{"question":"Are there differences between <C1>C1</C1> and <C2>C2</C2> that would tend to undermine the force of the similarity cited?","options":[]},{"question":"Is there some other case 3 that is also similar to <C1>C1</C1>, but in which <A>A</A> is true/false?","options":[]}]',
            'variables' => '["C1", "C2", "A", "OPTION1", "OPTION2"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 5,
            'name' => 'Correlation to Cause',
            'reasoning' => '["There is a <OPTION1>positive/negative/no</OPTION1> correlation between <A>A</A> and <B>B</B>.", "<A>A</A> <OPTION2>cause/does not cause</OPTION2> <B>B</B>."]',
            'critical_questions' => '[{"question":"Is there a positive/negative/no correlation between <A>A</A> and <B>B</B>?","options":["Positive","Negative","No"]},{"question":"Is there good evidence that the causal relationship goes from <A>A</A> to <B>B</B>, and not just from <B>B</B> to <A>A</A>?","options":["Evidence", "No Evidence"]},{"question":"Can it be ruled out that the correlation between <A>A</A> and <B>B</B> is accounted for by some third factor-a common cause-that causes both <A>A</A> and <B>B</B>?","options":["Agree","Disagree"]},{"question":"If there are intervening variables, can it be shown that the causal relationship between <A>A</A> and <B>B</B> is indirect-meditated through other causes?","options":["Agree","Disagree"]},{"question":"If the correlation fails to hold outside a certain range of causes, then can the limits of this range be clearly indicated?","options":[]},{"question":"Can it be shown that the change in <B>B</B> is not solely due to the way <B>B</B> is defined, the way entities are classified as belonging to the class of <B>B</B>, or changing standards, over time, of the way <B>B\'s</B> are defined?","options":[]},{"question":"Could there be some third factor,C, that is causing both <A>A</A> and <B>B</B>?","options":[]}]',
            'variables' => '["A", "B", "OPTION1", "OPTION2"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 6,
            'name' => 'Consequences',
            'reasoning' => '["If <A>A</A> is brought about, <OPTION1>good/bad</OPTION1> consequences will plausibly occur.", "<A>A</A> <OPTION2>should/should not</OPTION2> be brought about."]',
            'critical_questions' => '[{"question":"How strong is the probability that these cited consequences will may/might/must occur?","options":["may","might","must"]},{"question":"What evidence supports the claim that these consequences will may/might/must occur?","options":[]},{"question":"Are there consequences of the opposite value that ought to be taken into account?","options":[]},{"question":"Is <A>A</A> really good or bad?","options":["Good","Bad"]}]',
            'variables' => '["A", "OPTION1", "OPTION2"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 7,
            'name' => 'Causal Slippery Slope',
            'reasoning' => '["<A0>A0</A0> is up for consideration as a proposal that seems like it should be brought about.", "Bringing about <A0>A0</A0> would plausibly lead to <A1>A1</A1>, which would in turn lead to <A2>A2</A2>, and so forth through the sequence <A2>A2</A2>...<AN>AN</AN>.","<AN>AN</AN> is a <OPTION1>good/bad</OPTION1> outcome.", "<A0>A0</A0> <OPTION2>should/should not</OPTION2> be brought about."]',
            'critical_questions' => '[{"question":"Does the proponenet\'s description of the initial action <A0>A0</A0> rightly express the proposal being advocated by the respondent?","options":["Agree","Disagree"]},{"question":"Do any of the causual links in the sequence lack solid evidence to back it up as a causal claim?","options":["Evidence", "No Evidence"]},{"question":"Does this outcome plausibly follow from the sequence, and is it as the proponent suggests?","options":[]}]',
            'variables' => '["A0", "A1", "A2", "AN", "OPTION1", "OPTION2"]',
        ]);

        DB::table('argumentation_schemes')->insert([
            'id' => 8,
            'name' => 'Action',
            'reasoning' => '["In the current circumstances <R>R</R>,", "we should perform <A>action A</A>.", "Which will result in new circumstances <S>S</S>.", "Which will realise <G>goal G</G>.", "Which will promote some <V>value V</V>."]',
            'critical_questions' => '[{"question":"Are the believed <S>circumstances</S> true or false?","options":["True","False"]},{"question":"Does the <G>goal</G> realise the <V>value</V> stated?","options":["Agree","Disagree"]},{"question":"Are there alternative ways of realising the same <G>goal</G>?","options":["Agree","Disagree"]},{"question":"Does doing the <A>action</A> have a side effect which demotes or promotes <V>the value</V>?","options":["Demotes","Promotes"]},{"question":"Is <A>the action</A> possible?","options":["Agree","Disagree"]}]',
            'variables' => '["R", "A", "S", "G", "V"]',
        ]);

        DB::table('categories')->insert([
        	'id' => 1,
        	'name' => 'Politics'
        ]);
        DB::table('categories')->insert([
        	'id' => 9,
        	'name' => 'World'
        ]);
        DB::table('categories')->insert([
        	'id' => 10,
        	'name' => 'Technology'
        ]);
        DB::table('categories')->insert([
        	'id' => 11,
        	'name' => 'Business'
        ]);
        DB::table('categories')->insert([
        	'id' => 12,
        	'name' => 'Education'
        ]);
        DB::table('categories')->insert([
        	'id' => 13,
        	'name' => 'Health'
        ]);
        DB::table('categories')->insert([
        	'id' => 14,
        	'name' => 'Religion'
        ]);
        DB::table('categories')->insert([
        	'id' => 15,
        	'name' => 'Entertainment & Arts'
        ]);
        DB::table('categories')->insert([
        	'id' => 16,
        	'name' => 'Science'
        ]);
        DB::table('categories')->insert([
        	'id' => 17,
        	'name' => 'Sports'
        ]);

    }
}
