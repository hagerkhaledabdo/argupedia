<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriticalQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('critical_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->integer('argumentation_scheme_id')->unsigned();
            $table->timestamps();

            $table->foreign('argumentation_scheme_id')->references('id')->on('argumentation_schemes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('critical_questions');
    }
}
