<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArgumentationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("argumentation_schemes", function($table){
            $table->text("critical_questions")->nullable();
            $table->text("variables")->nullable();
        });

        Schema::table("arguments", function($table){
            $table->text("critical_questions")->nullable();
            $table->text("reasoning")->nullable();
            $table->text("variables")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("argumentation_schemes", function($table){
            $table->dropColumn("critical_questions");
            $table->dropColumn("variables");
        });

        Schema::table("arguments", function($table){
            $table->dropColumn("critical_questions");
            $table->dropColumn("reasoning");
            $table->dropColumn("variables");
        });
    }
}
