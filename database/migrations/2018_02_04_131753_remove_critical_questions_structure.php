<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCriticalQuestionsStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("arguments", function($table){
            $table->dropForeign(["question_id"]);
            $table->dropColumn("question_id");
        });

        Schema::dropIfExists("critical_questions");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("arguments", function($table){
            $table->integer('question_id')->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('critical_questions');
        });

        Schema::create('critical_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->integer('argumentation_scheme_id')->unsigned();
            $table->timestamps();

            $table->foreign('argumentation_scheme_id')->references('id')->on('argumentation_schemes');
        });
    }
}
